# Workshop NodeJS/AngularJS

## Roadmap

### Environment installation

- Install bower

		$ npm install -g bower

- Install express CLI

		$ npm install -g express-generator

- Init node.js express project

		$ express portail_parent
		$ cd portail_parent
		$ npm install

- Init front-end

		$ bower init
		$ bower install bootstrap --save
		$ bower install angularjs --save
		# Add bower_components to express routes as static files
		# Create index.html
		# Create scripts (app, providers, controllers, filters, directives, ...)

- Run application

		# Change port environment variable to 8080
		$ node ./bin/www

- Mongodb

MongoDB is preinstalled in your workspace. To run MongoDB, run the following below (passing the correct parameters to it). Mongodb data will be stored in the folder data.

	$ mkdir data
	$ echo 'mongod --bind_ip=$IP --dbpath=data --nojournal --rest "$@"' > mongod
	$ chmod a+x mongod
	
You can start mongodb by running the mongod script on your project root:

	$ ./mongod
	
MONGODB PARAMETERS USED:

	--dbpath=data - Because it defaults to /var/db (which isn't accessible)
	--nojournal - Because mongodb usually pre-allocates 2 GB journal file (which exceeds Cloud9 disk space quota)
	--bind_ip=$IP - Because you can't bind to 0.0.0.0
	--rest - Runs on default port 28017

	
Install moogoose

	$ npm install mongoose
			$ https://www.npmjs.com/package/mongoose
			
			
tuto toDo

http://adrianmejia.com/blog/2014/10/01/creating-a-restful-api-tutorial-with-nodejs-and-mongodb/
