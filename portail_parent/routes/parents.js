var express = require('express');
var router = express.Router();


var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('connected')
});

var parent = require('../models/parent.js');
/* GET home page. */
/* GET /todos listing. */
router.get('/', function(req, res, next) {
  parent.find( function (err, todos) {
    if (err) return next(err);
    res.json(todos);
  });
});

router.get('/test', function(req, res, next) {
  parent.find( function (err, todos) {
    if (err) return next(err);
    res.json({name:'test'});
  });
});

/* POST /todos */
router.post('/', function(req, res, next) {
  parent.create(req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* GET /todos/id */
router.get('/:id', function(req, res, next) {
  parent.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* PUT /todos/:id */
router.put('/:id', function(req, res, next) {
  parent.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* DELETE /todos/:id */
router.delete('/:id', function(req, res, next) {
  Todo.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;