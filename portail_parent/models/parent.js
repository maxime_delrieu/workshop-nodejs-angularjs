var mongoose = require('mongoose');

var enfant = new mongoose.Schema({
  nom: String,
  prenom: String,
  ddn: Date
});


var parent = new mongoose.Schema({
  nom: String,
  prenom: String,
  email: String,
  login: String,
  updated_at: { type: Date, default: Date.now },
  enfants:[enfant]
});

module.exports = mongoose.model('parent', parent);